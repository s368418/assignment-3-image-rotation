#ifndef IMAGE_TRNSF_BMP_H
#define IMAGE_TRNSF_BMP_H

#include <image.h>
#include <stdint.h>
#include <stdio.h>

#define BF_Type 19778
#define BFBIT_Count 24
#define BI_Size 40
#define BI_Planes 1
#define PELS 2834
#define STRUCT_SIZE sizeof(struct pixel)
#define BMP_HEADER_SIZE sizeof(struct bmp_header)

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bfOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

#include "image.h"
#include <stdio.h>

enum read_status  {
    READ_OK = 0,
    READ_ERROR,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS
};

enum read_status from_bmp(FILE* bmp_file, struct image* img);

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE* bmp_file, struct image const* img);

uint32_t get_padding(uint32_t width);

#endif

