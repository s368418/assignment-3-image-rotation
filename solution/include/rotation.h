#ifndef ROTATION_H
#define ROTATION_H

struct image rotate(struct image* source, int angle);

#endif
