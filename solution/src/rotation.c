#include <../include/image.h>
#include <../include/rotation.h>


struct image create_temp_image(size_t width, size_t height) {
    return create_image(width, height);
}


struct image rotate_0(struct image *source) {
    struct image rotated_clockwise = create_temp_image(source->width, source->height);
    if (rotated_clockwise.data) {
        struct pixel pixels;
        for (size_t i = 0; i < rotated_clockwise.height; i++) {
            for (size_t j = 0; j < rotated_clockwise.width; j++) {
                pixels = source->data[i * source->width + j];
                rotated_clockwise.data[i * source->width + j] = pixels;
            }
        }
    }
    return rotated_clockwise;

}



struct image rotate_90(struct image *source) {
    struct image rotated_clockwise = create_temp_image(source->height, source->width);
    if (rotated_clockwise.data) {
        struct pixel pixels;
        for (size_t i = 0; i < rotated_clockwise.height; i++) {
            for (size_t j = 0; j < rotated_clockwise.width; j++) {
                pixels = source->data[(j + 1) * source->width - i - 1];
                rotated_clockwise.data[i * rotated_clockwise.width + j] = pixels;
            }
        }
    }
    return rotated_clockwise;

}

struct image rotate_180(struct image *source) {
    struct image rotated_counter_clockwise = create_temp_image(source->width, source->height);
    if (rotated_counter_clockwise.data) {
        struct pixel pixels;
        for (size_t i = 0; i < rotated_counter_clockwise.height; i++) {
            for (size_t j = 0; j < rotated_counter_clockwise.width; j++) {
                pixels = source->data[i * source->width + j];
                rotated_counter_clockwise.data[source->height * source->width - i * source->width - j - 1] = pixels;
            }
        }
    }

    return rotated_counter_clockwise;
}

struct image rotate_270(struct image *source) {
    struct image rotated_clockwise = create_temp_image(source->height, source->width);
    if (rotated_clockwise.data) {
        struct pixel pixels;
        for (size_t i = 0; i < rotated_clockwise.width; i++) {
            for (size_t j = 0; j < rotated_clockwise.height; j++) {
                pixels = source->data[i * source->width + j];
                rotated_clockwise.data[(j + 1) * rotated_clockwise.width - i - 1] = pixels;
            }
        }
    }

    return rotated_clockwise;
}

struct image rotate(struct image *source, int angle) {
    struct image rotated_image;
    if (angle % 90 != 0) {
        rotated_image = (struct image) {.data = 0, .width = 0, .height = 0};
    } else if (angle == 0) {
        rotated_image = rotate_0(source);
    } else if (angle == 90 || angle == -270) {
        rotated_image = rotate_90(source);
    } else if (angle == 180 || angle == -180) {
        rotated_image = rotate_180(source);
    } else if (angle == 270 || angle == -90) {
        rotated_image = rotate_270(source);
    } else {
        rotated_image = *source;
    }

    return rotated_image;

}



