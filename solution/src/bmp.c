#include <../include/base.h>
#include <../include/bmp.h>
#include <stdlib.h>

enum read_status read_header(FILE* bmp_file, struct bmp_header* header) {
    if (fread(header, sizeof(struct bmp_header), 1, bmp_file) == 1) {
        if (header->bfType != BF_Type )
            return READ_INVALID_SIGNATURE;
        if (header->biBitCount != BFBIT_Count)
            return READ_INVALID_BITS;
        return READ_OK;
    }
    return READ_ERROR;
}

enum read_status read_pixels(FILE* bmp_file, struct image* img) {
    uint32_t padding = get_padding(img->width);

    for (size_t row = 0; row < img->height; ++row) {
        if (fread(img->data + row * img->width, STRUCT_SIZE, img->width, bmp_file) != img->width) {
            return READ_ERROR;
        }
        fseek(bmp_file, padding, SEEK_CUR);
    }

    return READ_OK;
}
enum read_status from_bmp(FILE* bmp_file, struct image* img) {
    struct bmp_header header;
    if (read_header(bmp_file, &header) != READ_OK) {
        return READ_ERROR;
    }

    *img = create_image(header.biWidth, header.biHeight);
    if (img->data == NULL) {
        return READ_ERROR;
    }

    // Прочитать пиксели из BMP-файла
    enum read_status pixels_status = read_pixels(bmp_file, img);
    if (pixels_status != READ_OK) {
        free(img->data);
        img->data = NULL;
        img->width = 0;
        img->height = 0;
    }

    return pixels_status;
}

enum write_status write_header(FILE* bmp_file, const struct image* img) {
    struct bmp_header header = {
            .bfType = BF_Type,
            .bfileSize = img->height * (img->width * STRUCT_SIZE + get_padding(img->width)) + BMP_HEADER_SIZE,
            .bfOffBits = BMP_HEADER_SIZE,
            .biSize = BI_Size,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BI_Planes,
            .biBitCount = BFBIT_Count,
            .biCompression = 0,
            .biSizeImage = img->height * (img->width * STRUCT_SIZE + get_padding(img->width)),
            .biXPelsPerMeter = PELS,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };

    if (fwrite(&header, sizeof(struct bmp_header), 1, bmp_file) == 1) {
        return WRITE_OK;
    }
    return WRITE_ERROR;
}

enum write_status write_pixels(FILE* bmp_file, const struct image* img) {
    uint32_t padding = get_padding(img->width);
    unsigned char padding_data[3] = {0};

    for (size_t row = 0; row < img->height; ++row) {
        if (fwrite(img->data + row * img->width, STRUCT_SIZE, img->width, bmp_file) != img->width) {
            return WRITE_ERROR;
        }
        if (padding > 0 && fwrite(padding_data, padding, 1, bmp_file) != 1) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}

enum write_status to_bmp(FILE* bmp_file, struct image const* img) {
    if (write_header(bmp_file, img) != WRITE_OK) {
        return WRITE_ERROR;

    }

    return write_pixels(bmp_file, img);

}

