#include <../include/bmp.h>
#include <../include/image.h>
#include <stdlib.h>

struct image create_image(const size_t width, const size_t height) {
    struct pixel* pixels = calloc(width * height, sizeof(struct pixel));
    if (pixels == NULL) {
        return (struct image){0, 0, NULL};
    }
    return (struct image){width, height, pixels};
}
uint32_t get_padding(uint32_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}



