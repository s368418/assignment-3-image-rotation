#include <../include/base.h>
#include <../include/bmp.h>
#include <../include/rotation.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    if (argc < 4) {
        fprintf(stderr, "Wrong input format. Should be 3 args");
        return 1;
    }

    FILE* input = open(argv[1], "rb");
    if (!input) {
        fprintf(stderr, "The input file does not exist or is not readable.");
        return 1;
    }

    int angle = atoi(argv[3]);
    if (angle % 90 != 0){
        fclose(input);
        fprintf(stderr, "Invalid angle. Angle must be a multiple of 90 degrees.");
        return 1;
    }

    struct image* img = malloc(sizeof(struct image));
    if (!img) {
        fclose(input);
        fprintf(stderr, "Memory allocation failed.");
        return 1;
    }

    if (from_bmp(input, img) != READ_OK) {
        fclose(input);
        fprintf(stderr, "Error reading image");
        free(img);
        return 1;
    }

    fclose(input);

    FILE* output = open(argv[2], "wb");
    if (!output) {
        fprintf(stderr, "The output file could not be created or opened.");
        free(img->data);
        free(img);
        return 1;
    }

    struct image rotated_image = rotate(img, angle);

    if (rotated_image.data == NULL) {
        fprintf(stderr, "Error rotating image");
        fclose(output);
        free(img->data);
        free(img);
        return 1;
    }

    if (to_bmp(output, &rotated_image) != WRITE_OK) {
        fprintf(stderr, "Error writing rotated image");
        fclose(output);
        free(img->data);
        free(img);
        free(rotated_image.data);
        return 1;
    }

    fclose(output);
    if (img->data) free(img->data);
    free(img);
    if (rotated_image.data) free(rotated_image.data);

    return 0;
}





