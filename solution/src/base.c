#include <../include/base.h>


FILE* open(char* file_name, char* mode) {
    FILE* file = fopen(file_name, mode);
    if (file == NULL) {
        fprintf(stderr, "File not found");
        return NULL;
    }
    return file;
}

